<?php include_once 'inc/top.php'; ?>

<?php
    $koriinlisaysoktk = false;
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        try {
            // ID piilokentästä
            $tuoteidtk = $_POST['tuoteid'];

            $kyselytk = $tietokantatk->query("SELECT * FROM tuote WHERE id = $tuoteidtk");
            $kyselytk->setFetchMode(PDO::FETCH_OBJ);
            $tietuetk = $kyselytk->fetch();
            
            $nimitk = $tietuetk->nimi;
            $kuvaustk = $tietuetk->kuvaus;
            $hintatk = $tietuetk->hinta;
            $kuvatk = $tietuetk->kuva;
            $tuoteryhmaidtk = $tietuetk->tuoteryhma_id;
            
            // Takaisin tuotteisiin -napin lähettämä GET-parametri
            $paluuidtk = $tuoteryhmaidtk;
            
            // Tuotteen tietojen sijoitus olioon ja olion laitto arrayhin
            $tuoteoliotk = new TuoteOlio($tuoteidtk, $nimitk, $kuvaustk, $hintatk, $kuvatk,$tuoteryhmaidtk);
            array_push($_SESSION['kori'], $tuoteoliotk);
            
            $koriinlisaysoktk = true;
        } catch (PDException $pdoextk) {
            $koriinlisaysoktk = false;
            ?><div class="alert alert-danger">
                <strong>Virhe.</strong> Tuotteen lisäys ostoskoriin epäonnistui; <?php print $pdoextk->getMessage(); ?>
            </div><?php
        }
    }

    if ($_SERVER["REQUEST_METHOD"] == "GET") {
        try {
            // Hakee tuotteen GET:in määrittelemän ID:n perusteella
            $tuoteidtk = $_GET['id'];

            $kyselytk = $tietokantatk->query("SELECT * FROM tuote WHERE id = $tuoteidtk");
            $kyselytk->setFetchMode(PDO::FETCH_OBJ);
            $tietuetk = $kyselytk->fetch();

            // Takaisin tuotteisiin -napin lähettämä GET-parametri
            $paluuidtk = $tietuetk->tuoteryhma_id;
        } catch (PDOException $pdoextk) {
            ?><div class="alert alert-danger">
                <strong>Virhe.</strong> Tuotteen haku tietokannasta epäonnistui; <?php print $pdoextk->getMessage(); ?>
            </div><?php
        }
    }
?>
            <div class="row">             
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- Tulostaa tuotteen nimen otsikoksi -->
                            <h3><?php print $tietuetk->nimi?></h3>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <!-- Tulostaa tuotteen kuvan -->
                            <?php print "<img class='center-block' src='products/".$tietuetk->kuva."' width='150' height='150'>"; ?>
                        </div>
                        <div class="col-xs-3">
                            <?php
                                // Tulostaa tuotteen nimen, hinnan ja kuvauksen
                                print "<p>".$tietuetk->nimi." ".$tietuetk->hinta." €</p>";
                                print "<p>".$tietuetk->kuvaus."</p>";
                            ?>
                        </div>
                        <div class="col-xs-3">
                            <!-- Ostoskoriin lisäys -nappi & id:n piilokenttä -->
                            <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
                                <input type="submit" class="btn btn-primary btn-block" value="Lisää ostoskoriin"><br>
                                <input type="hidden" id="tuoteid" name="tuoteid" value="<?php echo $tuoteidtk?>">
                            </form>
                            <!-- Paluu-nappi vie takaisin etusivuun niin, että aiemmin valittu tuoteryhmä tulee etusivulle valittuna -->
                            <input type="button" class="btn btn-default btn-block" onclick="window.location='index.php?tuoteryhmaid=<?php print $paluuidtk; ?>'; return false;" value="Takaisin tuotteisiin"></input>
                        </div>
                    </div>
                </div>
            </div>
            <?php 
                if ($_SERVER["REQUEST_METHOD"] == "POST") {
                    if ($koriinlisaysoktk === true) { ?>
                        <br><div class="alert alert-success">
                            Tuote lisätty ostoskoriin!
                        </div><?php
                    }
                }
            ?>
<?php include_once 'inc/bottom.php'; ?>