<?php

/**
 * Description of TuoteOlio
 *
 * @author Teemu
 */
class TuoteOlio {
    private $id;
    private $nimi;
    private $kuvaus;
    private $hinta;
    private $kuva;
    private $tuoteryhma_id;
    
    public function __construct($id, $nimi, $kuvaus, $hinta, $kuva, $tuoteryhma_id) {
        $this->id               = $id;
        $this->nimi             = $nimi;
        $this->kuvaus           = $kuvaus;
        $this->hinta            = $hinta;
        $this->kuva             = $kuva;
        $this->tuoteryhma_id    = $tuoteryhma_id;
    }
    
    function get_id() {
        return $this->id;
    }
    
    function get_nimi() {
        return $this->nimi;
    }
    
    function get_kuvaus() {
        return $this->kuvaus;
    }
    
    function get_hinta() {
        return $this->hinta;
    }
    
    function get_kuva() {
        return $this->kuva;
    }
    
    function get_tuoteryhma_id() {
        return $this->tuoteryhma_id;
    }
}
