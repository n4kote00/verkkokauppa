<div class="col-xs-6" style="text-align: right;">
                            <button class="btn btn-lg btn-info" data-toggle="collapse" data-target="#ostoskori">
                                <span class="glyphicon glyphicon-shopping-cart ostoskorinappi">
                                    <?php
                                        // Ostoskorin tuotteiden summan haku ja tulostus nappiin
                                        $korisummatk = 0;
                                        for ($i = 0; $i < count($_SESSION['kori']); $i++) {
                                            $tuoteclasstk = $_SESSION['kori'][$i];
                                            $korisummatk += $tuoteclasstk->get_hinta();
                                        }
                                        printf("%.2f eur",$korisummatk);
                                    ?>
                                </span>
                            </button>
                        </div>                        
                        <div class="col-xs-12" style="text-align: right;">
                            <div id="ostoskori" class="collapse ostoskori">
                                <table>
                                    <?php
                                        // Käy läpi arrayn ja tulostaa nimen ja hinnan, jotka haetaan oliosta
                                        for ($i = 0; $i < count($_SESSION['kori']); $i++) {
                                            $tuoteclasstk = $_SESSION['kori'][$i];
                                            echo "<tr>";
                                            echo "<td>".$tuoteclasstk->get_nimi()."</td>";
                                            printf("<td>%.2f €</td>", $tuoteclasstk->get_hinta());
                                            echo "<td><span class='glyphicon glyphicon-trash'></span></td>";
                                            echo "</tr>";
                                        }
                                    ?>
                                    <tr class="summa">
                                        <td>Summa</td>
                                        <!-- Summa alapalkissa -->
                                        <td><?php printf("%.2f €",$korisummatk); ?></td>
                                        <td></td>
                                    </tr>
                                </table>
                                <a href="order.php" class="btn btn-primary">
                                    Kassalle
                                </a>
                                <!-- Ostoskorin tyhjennys GET:in avulla -->
                                <button class="btn btn-default" onclick="window.location='index.php?empty'; return false;">
                                    Tyhjennä
                                </button>
                            </div>
                        </div>