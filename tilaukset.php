<?php include_once 'inc/top.php'; ?>


<div class="row">             
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-12">
                <h3>Tilaukset</h3>
                <hr>
            </div>
        </div>

<?php
    
?>

        <div class="row">
            <div class="col-xs-12">
                <hr>
                <table class="table table-responsive">
                    <tr>
                        <th>Tilaus</th>
                        <th>Sukunimi</th>
                        <th>Etunimi</th>
                        <th>Aika</th>
                        <th>Tuotteet</th>
                    </tr>
                    <tr>
                    <?php
                        try {
                            $kyselytk = $tietokantatk->query("SELECT "
                                    . "tilaus.id as tilausid, asiakas.sukunimi, asiakas.etunimi, tilaus.aika, tuote.nimi as tuotenimi "
                                    . "FROM asiakas INNER JOIN tilaus ON asiakas.id = tilaus.asiakas_id "
                                    . "INNER JOIN tilausrivi ON tilaus.id = tilausrivi.tilaus_id "
                                    . "INNER JOIN tuote ON tuote.id = tilausrivi.tuote_id");
                            $kyselytk->setFetchMode(PDO::FETCH_OBJ);
                            $tietuetk = $kyselytk->fetch();
                            
                            $check = 0;
                            
                            while($tietuetk = $kyselytk->fetch()){
                                print ("<tr>");
                                if ($check < $tietuetk->tilausid) {
                                    print ("<td>".$tietuetk->tilausid."</td>");
                                    print ("<td>".$tietuetk->sukunimi."</td>");
                                    print ("<td>".$tietuetk->etunimi."</td>");
                                    print ("<td>".$tietuetk->aika."</td>");
                                    print ("<td>".$tietuetk->tuotenimi."</td>");
                                    $check = $tietuetk->tilausid;
                                }
                                else {
                                    print ("<td></td>");
                                    print ("<td></td>");
                                    print ("<td></td>");
                                    print ("<td></td>");
                                    print ("<td>".$tietuetk->tuotenimi."</td>");
                                }
                                print ("</tr>");
                            }
                        } catch (PDOException $pdoex) {
                            print ($pdoex->getMessage());
                        }
                    ?>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<?php include_once 'inc/bottom.php'; ?>