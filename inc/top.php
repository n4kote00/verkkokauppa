<?php
include_once 'class/TuoteOlio.php';
session_start();
session_regenerate_id();

// Ostoskorin tyhjennys
if (isset($_GET['empty'])) {
    unset($_SESSION['kori']);
}

// Ostoskorin luonti
if (!isset($_SESSION['kori'])) {
    $_SESSION['kori'] = array();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <title>Verkkokauppa</title>
    </head>
    
    <?php
        try {
            $tietokantatk = new PDO('mysql:host=localhost;dbname=verkkokauppa;charset=utf8', 'root', '');
            $tietokantatk->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $pdoextk) { ?>
            <!--print "<p>Häiriö järjestelmässä. Tietokantaa ei voida avata.</p><p>" . $pdoextk->getMessage() . "</p>";-->
            <div class="alert alert-danger">
                <strong>Häiriö järjestelmässä.</strong> Tietokantaa ei voida avata; <?php print $pdoextk->getMessage(); ?>
            </div>
        <?php }
    ?>
    
    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top">
          <div class="container">
            <div class="navbar-header">
                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
		</button>        
                <a class="navbar-brand" href="index.php">Verkkokauppa</a>
                <div class="pull-right dropdown">
                <a class="dropdown-toggle navbar-brand" data-toggle="dropdown" href="#" type="button" id="dropdownMenu0" aria-haspopup="true" aria-expanded="false">
                    <small>Ylläpito</small>
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu0">
                    <li><a href="tuoteryhma.php">Lisää tuoteryhmä</a></li>
                    <li><a href="tuote.php">Lisää tuote</a></li>
                    <li class="divider"></li>
                    <li><a href="tilaukset.php">Tilaukset</a></li>
                </ul>
                </div>
            </div>   
          </div>
        </nav>
    <div class="container content">