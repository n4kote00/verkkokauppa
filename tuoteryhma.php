<?php include_once 'inc/top.php'; ?>


<div class="row">             
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-12">
                <h3>Lisää tuoteryhmä</h3>
                <hr>
            </div>
        </div>

<?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        try {
            $tuoteryhmanimitk = filter_input(INPUT_POST, "nimi", FILTER_SANITIZE_STRING);
            
            $nimikyselytk = $tietokantatk->prepare("INSERT INTO tuoteryhma (nimi) VALUES (:tuoteryhmanimi)");

            $nimikyselytk->bindValue(":tuoteryhmanimi", $tuoteryhmanimitk, PDO::PARAM_STR);
            $nimikyselytk->execute();
            
            print "<p>Tuoteryhmä tallennettu</p>";
        } catch (PDOException $pdoextk) {
            print "<p>Tuoteryhmän tallennuksessa tapahtui virhe.</p><p>" . $pdoextk->getMessage() . "</p>";
        }
    }
?>

        <div class="row">
            <div class="col-xs-12">
                <form role="form" action="<?php print $_SERVER['PHP_SELF'];?>" method="post">
                    <div class="form-group">
                        <label for="nimi">Nimi</label>
                        <input type="text" name="nimi" class="form-control" id="nimi">
                    </div>
                    <button type="submit" class="btn btn-primary">Tallenna</button>
                    <input type="button" class="btn btn-default" onclick="window.location='index.php'; return false;" value="Peruuta"></input>
                </form>
            </div>
        </div>
    </div>
</div>
<?php include_once 'inc/bottom.php'; ?>