<?php include_once 'inc/top.php'; ?>

<?php
    if ($_SERVER['REQUEST_METHOD'] == "GET" && isset($_GET['tuoteryhmaid'])) {
        // ID GET:n perusteella
        $tuoteryhmaidtk = $_GET['tuoteryhmaid'];
    } else {
        // Jos GET:tiä ei ole, valitaan pienin ID tuoteryhmästä (tuloksena Kengät)
        try {
            $kyselytk = $tietokantatk->query("SELECT * FROM tuoteryhma ORDER BY id ASC LIMIT 1");
            $kyselytk->setFetchMode(PDO::FETCH_OBJ);
            $tietuetk = $kyselytk->fetch();
            $tuoteryhmaidtk = $tietuetk->id;
        } catch (PDOException $pdoextk) {
            print $pdoextk;
        }
    }
?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="dropdown">
                                <button class="btn btn-default btn-lg dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                  Tuotteet
                                  <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    <?php
                                        try {
                                            // Tulostaa tuotelistan
                                            $kyselytk = $tietokantatk->query("SELECT * FROM tuoteryhma");
                                            $kyselytk->setFetchMode(PDO::FETCH_OBJ);

                                            while($tietuetk = $kyselytk->fetch()){
                                                print "<li><a href='".$_SERVER['PHP_SELF']."?tuoteryhmaid=".$tietuetk->id."'>" . $tietuetk->nimi . "</a></li>";
                                            }
                                        } catch (PDOException $pdoextk) {
                                            print $pdoextk;
                                        }
                                    ?>
                                </ul>
                            </div>
                        </div>
                        <!-- Ostoskori -->
                        <?php include_once 'kori.php';?>
                    </div>
                </div>
            </div>

<div class="row">
    <div class="col-xs-12 tuotteet">
        <div class="row">
<?php

    try {
        // Hakee ja tulostaa tuoteryhmän otsikon
        $kyselytk1 = $tietokantatk->query("SELECT nimi FROM tuoteryhma WHERE id = $tuoteryhmaidtk");
        $kyselytk1->setFetchMode(PDO::FETCH_OBJ);
        $tietuetk = $kyselytk1->fetch();
        print "<div class='col-xs-12'>";
        print "<h4>".$tietuetk->nimi."</h4>";
        print "<hr></div></div><div class='row'>";

        // Hakee ja tulostaa tuoteryhmän tuotteet
        $kyselytk2 = $tietokantatk->query("SELECT * FROM tuote WHERE tuoteryhma_id = $tuoteryhmaidtk");
        $kyselytk2->setFetchMode(PDO::FETCH_OBJ);

        while($tietuetk = $kyselytk2->fetch()){
            print "<div class='col-xs-6 col-sm-4 col-md-3 tuote'>";
            print "<p>";
            // Kuvan width ja height asetetaan 150 pikseliin 100% sijasta, koska 100% aiheutti erikokoisien kuvien kanssa ongelmia
            print "<a href='tuotetieto.php?id=".$tietuetk->id."'><img src='products/".$tietuetk->kuva."' width='150' height='150'></a>";
            print "<br/>";
            print "$tietuetk->kuvaus";
            print "<br/>";
            print "$tietuetk->hinta";
            print "</p>";
            print "</div>";
        }
    } catch (PDOException $pdoextk) {
        print $pdoextk;
    }
?>
        </div>
    </div>
</div>
<?php include_once 'inc/bottom.php'; ?>