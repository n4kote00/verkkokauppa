<?php include_once 'inc/top.php'; ?>

<?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $korityhja = false;
        if (!empty($_SESSION['kori'])) {
            try {
            $asiakasetunimi = filter_input(INPUT_POST, "etunimi", FILTER_SANITIZE_STRING);
            $asiakassukunimi = filter_input(INPUT_POST, "sukunimi", FILTER_SANITIZE_STRING);
            $asiakasosoite = filter_input(INPUT_POST, "lahiosoite", FILTER_SANITIZE_STRING);
            $asiakastoimipaikka = filter_input(INPUT_POST, "postitoimipaikka", FILTER_SANITIZE_STRING);
            $asiakaspostinumero = filter_input(INPUT_POST, "postinumero", FILTER_SANITIZE_STRING);
            $asiakaspuhelin = filter_input(INPUT_POST, "puhelin", FILTER_SANITIZE_STRING);
            
            $tietokantatk->beginTransaction();
            
            try {
                $sql1 = "INSERT INTO asiakas "
                    . "(etunimi, sukunimi, lahiosoite, postitoimipaikka, postinumero, puhelin)"
                    . " VALUES "
                    . "(:etunimi, :sukunimi, :lahiosoite, :postitoimipaikka, :postinumero, :puhelin)";
                $kyselytk = $tietokantatk->prepare($sql1);
                $kyselytk->bindValue(':etunimi', $asiakasetunimi, PDO::PARAM_STR);
                $kyselytk->bindValue(':sukunimi', $asiakassukunimi, PDO::PARAM_STR);
                $kyselytk->bindValue(':lahiosoite', $asiakasosoite, PDO::PARAM_STR);
                $kyselytk->bindValue(':postitoimipaikka', $asiakastoimipaikka, PDO::PARAM_STR);
                $kyselytk->bindValue(':postinumero', $asiakaspostinumero, PDO::PARAM_STR);
                $kyselytk->bindValue(':puhelin', $asiakaspuhelin, PDO::PARAM_STR);
                $kyselytk->execute();
            } catch (PDOException $pdoex) {
                print($pdoex->getMessage());
            }
            
            $asiakasidtk = $tietokantatk->lastInsertId();
            
            try {
                $kyselytk = $tietokantatk->prepare("INSERT INTO tilaus (asiakas_id) VALUES (:asiakas_id)");
                $kyselytk->bindValue(':asiakas_id', $asiakasidtk, PDO::PARAM_STR);
                $kyselytk->execute();
            } catch (PDOException $pdoex) {
                print($pdoex->getMessage());
            }
            
            $tilausidtk = $tietokantatk->lastInsertId();
            
            $ostoskoritk = $_SESSION['kori'];  
            
            foreach ($ostoskoritk as $tuote) {
                try {
                    $sql2 = "INSERT INTO tilausrivi (tilaus_id, tuote_id) VALUES (:tilaus_id,:tuote_id)";
                    $kyselytk = $tietokantatk->prepare($sql2);
                    $kyselytk->bindValue(':tilaus_id', $tilausidtk, PDO::PARAM_STR);
                    $kyselytk->bindValue(':tuote_id', $tuote->get_id(), PDO::PARAM_STR);
                    $kyselytk->execute();
                } catch (PDOException $pdoex) {
                    print($pdoex->getMessage());
                }
            }
            $actionOKtk = true;
            } catch (PDOException $pdoex) {
                $actionOKtk = false;
                print("Epäonnistui: ". $pdoex);
            }

            if ($actionOKtk == true) {
                $tietokantatk->commit();
                unset($_SESSION['ostoskori']);
                session_destroy();
                //print("Success");
            }
            else {
                $tietokantatk->rollBack();
                //print("Failure");
            }
        }
        else {
            $korityhja = true;
            $actionOKtk = false;
        }
    }
?>

<div class="row">             
    <div class="col-xs-12 tilaus">
        <div class="row">
            <div class="col-xs-12">
                <h4>Tilaus</h4>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-8 col-lg-6">
                <form role="form" action="<?php print $_SERVER['PHP_SELF'];?>" method="post">
                    <div class="form-group">
                        <label>Sukunimi</label>
                        <input name="sukunimi" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Etunimi</label>
                        <input name="etunimi" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Lähiosoite</label>
                        <input name="lahiosoite" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Postitoimipaikka</label>
                        <input name="postitoimipaikka" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Postinumero</label>
                        <input name="postinumero" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Puhelin</label>
                        <input name="puhelin" class="form-control" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Lähetä</button>
                    <input type="button" class="btn btn-default" onclick="window.location='index.php'; return false;" value="Peruuta"></input>
                </form>
            </div>
        </div>
    </div>
</div>

<?php 
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if ($actionOKtk == true) {
            ?>
            <br>
            <div class="alert alert-success">
                <strong>Tilattu.</strong> Kiitos tilauksesta. <a href="index.php">Takaisin tuotteisiin.</a>
            </div>
            <?php
        }
        else {
            if ($korityhja === true) { ?>
                <br>
                <div class="alert alert-danger">
                    <strong>Virhe.</strong> Ostoskorisi on tyhjä. <a href="index.php">Takaisin tuotteisiin.</a>
                </div><?php
            } else {
            ?>
            <br>
            <div class="alert alert-danger">
                <strong>Virhe.</strong> Tilauksen kanssa tapahtui virhe. <a href="index.php">Takaisin tuotteisiin.</a>
            </div>
            <?php }
        }
    }
?>

<?php include_once 'inc/bottom.php'; ?>