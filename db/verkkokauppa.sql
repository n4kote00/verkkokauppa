-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 15.01.2016 klo 20:13
-- Palvelimen versio: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `verkkokauppa`
--

-- --------------------------------------------------------

--
-- Rakenne taululle `asiakas`
--

CREATE TABLE IF NOT EXISTS `asiakas` (
  `id` int(11) NOT NULL,
  `etunimi` varchar(50) NOT NULL,
  `sukunimi` varchar(50) NOT NULL,
  `lahiosoite` varchar(50) NOT NULL,
  `postitoimipaikka` varchar(30) NOT NULL,
  `postinumero` varchar(15) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `puhelin` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Rakenne taululle `tilaus`
--

CREATE TABLE IF NOT EXISTS `tilaus` (
  `id` int(11) NOT NULL,
  `aika` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `asiakas_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Rakenne taululle `tilausrivi`
--

CREATE TABLE IF NOT EXISTS `tilausrivi` (
  `tilaus_id` int(11) NOT NULL,
  `tuote_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Rakenne taululle `tuote`
--

CREATE TABLE IF NOT EXISTS `tuote` (
  `id` int(11) NOT NULL,
  `nimi` varchar(50) NOT NULL,
  `kuvaus` text,
  `hinta` decimal(5,2) NOT NULL,
  `kuva` varchar(30) DEFAULT NULL,
  `tuoteryhma_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Vedos taulusta `tuote`
--

INSERT INTO `tuote` (`id`, `nimi`, `kuvaus`, `hinta`, `kuva`, `tuoteryhma_id`) VALUES
(5, 'aa', 'bb', '1.00', 'IMAG00240.86815900 1448485932.', 9),
(6, 'bb', 'ss', '50.25', 'IMAG00270.52503800 1448486089.', 8),
(7, 'fdasfdsa', 'fdsa', '55.00', '', 6),
(8, 'dsa', 'dsa', '54.00', '', 9),
(9, 'DAdsadsa', 'dsa', '55.00', '', 9),
(10, 'das', 'dsa', '55.00', '', 9);

-- --------------------------------------------------------

--
-- Rakenne taululle `tuoteryhma`
--

CREATE TABLE IF NOT EXISTS `tuoteryhma` (
  `id` int(11) NOT NULL,
  `nimi` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Vedos taulusta `tuoteryhma`
--

INSERT INTO `tuoteryhma` (`id`, `nimi`) VALUES
(9, 'Housut'),
(6, 'Kengät'),
(10, 'Lakit'),
(11, 'Sukkia'),
(8, 'Takit');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `asiakas`
--
ALTER TABLE `asiakas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tilaus`
--
ALTER TABLE `tilaus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `asiakas_id` (`asiakas_id`);

--
-- Indexes for table `tilausrivi`
--
ALTER TABLE `tilausrivi`
  ADD KEY `tilaus_id` (`tilaus_id`),
  ADD KEY `tuote_id` (`tuote_id`);

--
-- Indexes for table `tuote`
--
ALTER TABLE `tuote`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tuoteryhma_id` (`tuoteryhma_id`);

--
-- Indexes for table `tuoteryhma`
--
ALTER TABLE `tuoteryhma`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nimi` (`nimi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `asiakas`
--
ALTER TABLE `asiakas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tilaus`
--
ALTER TABLE `tilaus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tuote`
--
ALTER TABLE `tuote`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tuoteryhma`
--
ALTER TABLE `tuoteryhma`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- Rajoitteet vedostauluille
--

--
-- Rajoitteet taululle `tilaus`
--
ALTER TABLE `tilaus`
  ADD CONSTRAINT `tilaus_ibfk_1` FOREIGN KEY (`asiakas_id`) REFERENCES `asiakas` (`id`);

--
-- Rajoitteet taululle `tilausrivi`
--
ALTER TABLE `tilausrivi`
  ADD CONSTRAINT `tilausrivi_ibfk_1` FOREIGN KEY (`tilaus_id`) REFERENCES `tilaus` (`id`),
  ADD CONSTRAINT `tilausrivi_ibfk_2` FOREIGN KEY (`tuote_id`) REFERENCES `tuote` (`id`);

--
-- Rajoitteet taululle `tuote`
--
ALTER TABLE `tuote`
  ADD CONSTRAINT `tuote_ibfk_1` FOREIGN KEY (`tuoteryhma_id`) REFERENCES `tuoteryhma` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

