<?php include_once 'inc/top.php'; ?>


<div class="row">             
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-12">
                <h3>Lisää tuote</h3>
                <hr>
            </div>
        </div>

<?php

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $latausOktk = false;

        if ($_FILES['kuvatiedosto']['error'] == UPLOAD_ERR_OK) {
            $kuvatiedostotk = $_FILES['kuvatiedosto']['name'];
            if ($_FILES['kuvatiedosto']['size'] > 0) {
                $tyyppitk = $_FILES['kuvatiedosto']['type'];
                if (strcmp($tyyppitk, "image/jpeg") == 0 || strcmp($tyyppitk, "image/pjpeg") == 0) {
                    $kuvatiedostotk = basename($kuvatiedostotk);
                    $kansiotk = 'products/';
                    
                    $kohdetk = $kansiotk.$kuvatiedostotk;
                    
                    // Tarkastaa, onko samannimisiä tiedostoja. Jos on, lisää numeron tiedoston perään.
                    if (file_exists($kohdetk)) {
                        $path_partstk = pathinfo($kohdetk);
                        $kuvanrotk = 2;
                        while (file_exists($kansiotk.$path_partstk['filename']."_".$kuvanrotk.".".$path_partstk['extension'])) {
                            $kuvanrotk++;
                        }
                        $kohdetk = $kansiotk.$path_partstk['filename']."_".$kuvanrotk.".".$path_partstk['extension'];
                        $kuvatiedostotk = $path_partstk['filename']."_".$kuvanrotk.".".$path_partstk['extension'];
                    }
                    
                    // Siirtää kuvan kansioon
                    move_uploaded_file($_FILES["kuvatiedosto"]["tmp_name"], $kohdetk);
                    $latausOktk = true;
                }
                else { 
                    ?>
                    <div class="alert alert-warning">
                        <strong>Väärä tiedostotyyppi!</strong> Voit ladata vain jpg-kuvia
                    </div>
                    <?php 
                }
            } else { 
                ?>
                <div class="alert alert-warning">
                    <strong>Tyhjä tiedostotyyppi!</strong> Kuvan koko ei kelpaa
                </div>
                <?php 
            }
        } else { 
            ?>
            <div class="alert alert-warning">
                <strong>Virhe!</strong> Kuvan lataus epäonnistui
            </div>
            <?php 
        }
        
        if ($latausOktk === true) {
            try {
                // Vie kuvan tiedot tietokantaan
                $nimitk = filter_input(INPUT_POST, "nimi", FILTER_SANITIZE_STRING);
                $kuvaustk = filter_input(INPUT_POST, "kuvaus", FILTER_SANITIZE_STRING);
                $hintatk = filter_input(INPUT_POST, "hinta", FILTER_SANITIZE_NUMBER_INT);
                $tuoteryhmatk = $_POST["tuoteryhma"];

                $tuotekyselytk = $tietokantatk->prepare("INSERT INTO tuote (nimi, kuvaus, hinta, kuva, tuoteryhma_id) VALUES (:nimi, :kuvaus, :hinta, :kuva, :tuoteryhma_id)");

                $tuotekyselytk->bindValue(":nimi", $nimitk, PDO::PARAM_STR);
                $tuotekyselytk->bindValue(":kuvaus", $kuvaustk, PDO::PARAM_STR);
                $tuotekyselytk->bindValue(":hinta", $hintatk, PDO::PARAM_STR);
                $tuotekyselytk->bindValue(":kuva", $kuvatiedostotk, PDO::PARAM_STR);
                $tuotekyselytk->bindValue(":tuoteryhma_id", $tuoteryhmatk, PDO::PARAM_STR);
                $tuotekyselytk->execute();

                ?>
                <div class="alert alert-success">
                    <strong>Lähetetty!</strong> Uuden tuotteen luonti onnistui
                </div>
                <?php 
            } catch (PDOException $pdoextk) {
                ?>
                <div class="alert alert-warning">
                    <strong>Virhe!</strong> Tietokannan kanssa tapahtui virhe: <?php $pdoextk->getMessage(); ?>
                </div>
                <?php
            }
        }
    }
?>

        <div class="row">
            <div class="col-xs-12">
                <form role="form" action="<?php print $_SERVER['PHP_SELF'];?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="tuoteryhma">Tuoteryhmä</label>
                        <select class="form-control" id="tuoteryhma" name="tuoteryhma">
                            <?php
                                try {
                                    // Tulostaa tuoteryhmävaihtoehdot
                                    $kyselytk = $tietokantatk->query("SELECT * FROM tuoteryhma");
                                    $kyselytk->setFetchMode(PDO::FETCH_OBJ);

                                    while($tietuetk = $kyselytk->fetch()){
                                        print "<option value='". $tietuetk->id. "'>" . $tietuetk->nimi . "</option>";
                                    }
                                } catch (PDOException $pdoextk) {
                                    print $pdoextk;
                                }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="nimi">Nimi</label>
                        <input type="text" name="nimi" class="form-control" id="nimi">
                    </div>
                    <div class="form-group">
                        <label for="kuvaus">Kuvaus</label>
                        <textarea type="text" name="kuvaus" class="form-control" rows="2" id="kuvaus"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="kuvatiedosto">Kuvatiedosto</label>
                        <input type="file" name="kuvatiedosto" class="form-control" id="kuvatiedosto">
                    </div>
                    <div class="form-group">
                        <label for="hinta">Hinta</label>
                        <input type="number" name="hinta" class="form-control" id="hinta">
                    </div>
                    <button type="submit" class="btn btn-primary">Lähetä</button>
                    <input type="button" class="btn btn-default" onclick="window.location='index.php'; return false;" value="Peruuta"></input>
                </form>
            </div>
        </div>
    </div>
</div>
<?php include_once 'inc/bottom.php'; ?>